package com.mgr.actors

import java.time.LocalDate

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.stream.ActorMaterializer
import com.hazelcast.core.{IMap, IdGenerator}
import com.hazelcast.mapreduce.aggregation.impl.KeyPredicateSupplier
import com.hazelcast.query.PredicateBuilder
import com.mgr.actors.FileInfoRepository.{CreateFileInfo, GetFile, ReplaceFileInfo}
import com.mgr.exceptions.{FileInfoExists, FileInfoNotFound}
import com.mgr.grid.{FileChunk, FileInfo}

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContextExecutor

object FileInfoRepository {

  case class GetFile(fileName: String)

  case class ReplaceFileInfo(fileName: FileInfo, numberOfChunks: Long)

  case class CreateFileInfo(fileName: String)

  def props(fileInfoMap: IMap[Long, FileInfo], fileInfoIdGenerator: IdGenerator): Props =
    Props(new FileInfoRepository(fileInfoMap, fileInfoIdGenerator))
}

trait FileInfoHazelcast {
  implicit val fileInfoMap: IMap[Long, FileInfo]
  implicit val fileInfoIdGen: IdGenerator

  def getByName(fileName: String): Option[FileInfo] = {
    val predicate = new PredicateBuilder().getEntryObject().get("fileName").equal(fileName)
    val values = fileInfoMap.values(predicate).asScala
    values.headOption

    //    val query = new TextQuery[Long, FileInfo](classOf[FileInfo], fileName) //ignite
    //    val fileInfoResult = fileInfoMap.query(query).asScala.map(_.getValue).headOption
  }

  def create(fileName: String): Option[FileInfo] = {
    val fileInfo = FileInfo(fileInfoIdGen.newId(), 0, fileName, -1, LocalDate.now())
    val absent = fileInfoMap.putIfAbsent(fileInfo.id, fileInfo)

    if (absent == null) {
      return Some(fileInfo)
    }
    return None
    //    val fileInfo = FileInfo(fileInfoSeq.getAndIncrement(), 0, fileName, -1, LocalDate.now())
    //    val info = fileInfoCache.putIfAbsent(fileInfo.id, fileInfo)
  }

  def replace(current:FileInfo, newValue:FileInfo) = {
    fileInfoMap.replace(current.id, current, newValue)
  }
}

class FileInfoRepository(fileInfoMapHz: IMap[Long, FileInfo], fileInfoIdGenerator: IdGenerator)
  extends Actor with ActorLogging with FileInfoHazelcast {

  implicit val system: ActorSystem = context.system
  implicit val executionContext: ExecutionContextExecutor = context.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val fileInfoMap = fileInfoMapHz
  implicit val fileInfoIdGen = fileInfoIdGenerator

//  private def getCacheConfig = {
//    val cacheConfig: CacheConfiguration[Long, FileInfo] = new CacheConfiguration[Long, FileInfo]()
//    cacheConfig.setIndexedTypes(classOf[Long], classOf[FileInfo])
//    cacheConfig.setName(CacheNames.fileInfoCache)
//    cacheConfig
//  }

//  private val fileInfoCache = scalar.ignite$.getOrCreateCache[Long, FileInfo](getCacheConfig)
//  private val fileInfoSeq = scalar.ignite$.atomicSequence(SequenceNames.fileInfoSeq, 0, true)

  override def receive: Receive = {
    case GetFile(fileName) =>
      //      val query = new TextQuery[Long, FileInfo](classOf[FileInfo], fileName)
      //      val fileInfoResult = fileInfoCache.query(query).asScala.map(_.getValue).headOption
      val fileInfoResult = getByName(fileName)
      fileInfoResult match {
        case Some(fileInfo) =>
          sender() ! fileInfo
        case _ =>
          sender() ! FileInfoNotFound()
      }

    case CreateFileInfo(fileName) =>
      val result = create(fileName)
      result match {
        case Some(fileInfo) =>
          sender() ! fileInfo
        case None =>
          sender() ! FileInfoExists()
      }

    case ReplaceFileInfo(fileInfo, numberOfChunks) =>
      val updatedInfo = fileInfo.copy(numberOfChunks = numberOfChunks)
      replace(fileInfo, updatedInfo)
//      fileInfoCache.put(updatedInfo.id, updatedInfo) //ignite

    case _ =>
      sender() ! FileInfoNotFound()
  }
}
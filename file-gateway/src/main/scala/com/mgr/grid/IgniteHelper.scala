//package com.mgr.grid
//
////import org.apache.ignite.configuration.{CacheConfiguration, IgniteConfiguration}
////import org.apache.ignite.events.EventType.{EVT_CACHE_OBJECT_PUT, EVT_CACHE_OBJECT_READ, EVT_CACHE_OBJECT_REMOVED}
////import org.apache.ignite.logger.slf4j.Slf4jLogger
////import org.apache.ignite.scalar.scalar
////import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi
////import org.apache.ignite.spi.discovery.tcp.ipfinder.multicast.TcpDiscoveryMulticastIpFinder
////import org.apache.ignite.{Ignite, IgniteAtomicSequence, IgniteCache}
////
////import scala.collection.JavaConverters._
//
//object CacheNames {
//  val chunkCache: String = "chunkGrid"
//  val fileInfoCache: String = "fileInfoGrid"
//}
//
//object SequenceNames {
//  val fileInfoSeq: String = "fileInfoSeq"
//}
//
//object IgniteHelper {
//
////  var ignite: Ignite = _
////  var chunkCache: IgniteCache[FileChunkKey, FileChunk] = _
////  var fileInfoCache: IgniteCache[Long, FileInfo] = _
////  var fileInfoSeq: IgniteAtomicSequence = _
//
//  def init(): Unit = {
//    start()
//    createChunkCache()
//    createFileInfoCache()
//  }
//
//  private def start() = {
//    ignite = scalar.start(createIgniteConfig())
//  }
//
//  private def createIgniteConfig() = {
//    val igniteConfiguration = new IgniteConfiguration()
//    igniteConfiguration.setClientMode(true)
//
////    val multicastIpFinder = new TcpDiscoveryMulticastIpFinder()
////    multicastIpFinder.setAddresses(Seq("127.0.0.1:47500..47509").asJava)
////    igniteConfiguration.setDiscoverySpi(new TcpDiscoverySpi().setIpFinder(multicastIpFinder))
////    igniteConfiguration.setGridLogger(new Slf4jLogger())
//
//    igniteConfiguration
//  }
//
//  private def createChunkCache() = {
//    val chunkCacheConfig: CacheConfiguration[FileChunkKey, FileChunk] = new CacheConfiguration[FileChunkKey, FileChunk]()
//    chunkCacheConfig.setIndexedTypes(classOf[FileChunkKey], classOf[FileChunk])
//    chunkCacheConfig.setName(CacheNames.chunkCache)
//
//    chunkCache = ignite.getOrCreateCache[FileChunkKey, FileChunk](chunkCacheConfig)
//  }
//
//  private def createFileInfoCache() = {
//    val cacheConfig: CacheConfiguration[Long, FileInfo] = new CacheConfiguration[Long, FileInfo]()
//    cacheConfig.setIndexedTypes(classOf[Long], classOf[FileInfo])
//    cacheConfig.setName(CacheNames.fileInfoCache)
//
//    fileInfoCache = ignite.getOrCreateCache[Long, FileInfo](cacheConfig)
//    fileInfoSeq = ignite.atomicSequence(SequenceNames.fileInfoSeq, 0, true)
//  }
//}


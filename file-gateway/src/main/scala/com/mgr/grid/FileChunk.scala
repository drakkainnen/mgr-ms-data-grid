package com.mgr.grid

import java.time.LocalDate


case class FileInfo(id: Long,
                    userId: Long,
                    fileName: String,
                    numberOfChunks: Long,
                    creationDate: LocalDate)

case class FileChunkKey(id: Long, fileInfo: Long)

case class FileChunk(id: Long, fileInfoId: Long, bytes: Seq[Byte])

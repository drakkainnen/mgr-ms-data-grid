package com.mgr.util

/**
  * Created by BBUDNIK on 02.07.2017.
  */
object StorageType extends Enumeration {
  type StorageType = Value
  val hz, file = Value
}

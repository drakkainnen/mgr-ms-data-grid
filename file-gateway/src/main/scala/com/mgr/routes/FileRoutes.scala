package com.mgr.routes

import java.nio.file.Paths

import akka.NotUsed
import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Route, directives}
import akka.pattern.ask
import akka.stream._
import akka.stream.scaladsl._
import akka.util.{ByteString, Timeout}
import com.hazelcast.core.IMap
import com.mgr.actors.FileInfoRepository.{CreateFileInfo, GetFile, ReplaceFileInfo}
import com.mgr.actors._
import com.mgr.exceptions.{ChunkExistException, HttpUserException}
import com.mgr.grid._

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.util.{Failure, Success}

trait FileRoutes {

  import Protocols.fileRequestFormat

  implicit val materializer: Materializer
  implicit val executionContext: ExecutionContextExecutor
  implicit val system: ActorSystem
  implicit val timeout = Timeout(2.second)

  implicit val chunkMap: IMap[FileChunkKey, FileChunk]

  def fileRoutes(fileInfoRepository: ActorRef): Route = {
    pathPrefix("file") {
      uploadFromDirective(fileInfoRepository) ~
        getFile(fileInfoRepository)
    }
  }

  def fileFsRoutes(): Route = {
    pathPrefix("file") {
      uploadFromDirectiveToFS() ~ getFileFs()
    }
  }

  def getFileFs(): Route = {
    (post & pathEndOrSingleSlash) {
      entity(as[FileRequest]) {
        case FileRequest(fileName) =>
          val source = FileIO.fromPath(Paths.get("/tmp/gw/") resolve fileName)
          complete(HttpEntity(ContentTypes.`application/octet-stream`, source))
      }
    }
  }

  //curl -v -d '{"fileName": "name"}' -H 'Content-Type: application/json' 127.0.0.1:8888/api/file
  private def getFile(fileInfoRepository: ActorRef): Route = {
    (post & pathEndOrSingleSlash) {
      entity(as[FileRequest]) {
        case FileRequest(fileName) =>

          val fileInfoAsk = fileInfoRepository ? GetFile(fileName)
          onSuccess(fileInfoAsk) {
            case fileInfo: FileInfo =>
              getFileData(fileInfo)
            case ex: HttpUserException =>
              complete(ex.statusCode -> ex.message)
          }
      }
    }
  }

  private def getFileData(fileInfo: FileInfo) = {
    val source = Source.fromGraph(getFileFromChunks(fileInfo))
    complete(HttpEntity(ContentTypes.`application/octet-stream`, source))
  }


  private def uploadFromDirectiveToFS() = {
    (post & path("upload2")) {
      withoutSizeLimit {
        extractLog {
          implicit log =>
            fileUpload("file") {
              case (metadata, byteSource) =>
                val sink = FileIO.toPath(Paths.get("/tmp/gw/") resolve metadata.fileName)
                val writeResult = byteSource.runWith(sink)
                onSuccess(writeResult) { result =>
                  result.status match {
                    case Success(_) => complete(s"Successfully written ${result.count} bytes")
                    case Failure(e) => throw e
                  }
                }
            }
        }
      }
    }
  }
  //cd ~/Desktop/ngrams
  //curl -v -F file=@2grams 127.0.0.1:8888/api/file/upload2
  private def uploadFromDirective(fileInfoRepository: ActorRef) = {
    (post & path("upload2")) {
      withoutSizeLimit {
        extractLog {
          implicit log =>
            fileUpload("file") {
              case (metadata, byteSource) =>
                saveFileChunks(fileInfoRepository, metadata, byteSource)
            }
        }
      }
    }
  }

  private def saveFileChunks(fileInfoRepository: ActorRef,
                             metadata: directives.FileInfo,
                             byteSource: Source[ByteString, Any]) = {
    val fileCreated = fileInfoRepository ? CreateFileInfo(metadata.fileName)
    onSuccess(fileCreated) {
      case fileInfo: FileInfo =>
        val graph = RunnableGraph.fromGraph(fileUploadGraph(byteSource, fileInfo.id))
        val savedChunks = graph.run()

        onSuccess(savedChunks) {
          lastChunkNumber =>
            fileInfoRepository ! ReplaceFileInfo(fileInfo, lastChunkNumber)
            complete(lastChunkNumber.toString)
        }
    }
  }

  private def fileUploadGraph(byteSource: Source[ByteString, Any],
                              fileInfoId: Long) = {

    def saveChunk(data: (ByteString, Long)): Long = {
      val value = FileChunk(data._2, fileInfoId, data._1)
      //      chunkRepository.saveChunk(value) ignite
      //below hz
      val success = chunkMap.putIfAbsent(FileChunkKey(value.id, value.fileInfoId), value)
      println(value)
      if (success != null) {
        throw new ChunkExistException()
      }
      value.id
    }

    GraphDSL.create(Sink.last[Long]) {
      implicit builder =>
        sink =>
          import GraphDSL.Implicits._

          val indexer = Source.fromIterator(() => Iterator.iterate(0L: Long)(_ + 1L))
          val zip = builder.add(Zip[ByteString, Long]())

          byteSource ~> zip.in0
          indexer ~> zip.in1

          zip.out.map(saveChunk(_)) ~> sink
          ClosedShape
    }
  }

  private def getFileFromChunks(fileInfo: FileInfo): Graph[SourceShape[ByteString], NotUsed] = {

    def getChunk(number: Long, fileInfo:FileInfo): ByteString = {
      val key = FileChunkKey(number, fileInfo.id)
      val chunk = chunkMap.get(key)
      if(chunk == null) {
        throw new IllegalArgumentException(number.toString)
      }
      //      val bs = ByteString(chunkRepository.getChunk.get(key).bytes.toArray) //ignite
      val bs = ByteString(chunk.bytes.toArray) //hz
      bs
    }

    def getIterator(fileInfo: FileInfo): Iterator[ByteString] = {
      Iterator
        .iterate[Long](0L)(_ + 1L)
        .takeWhile(_ <= fileInfo.numberOfChunks)
        .map(getChunk(_, fileInfo))
    }

    GraphDSL.create() {
      implicit builder =>

        val indexer = Source.fromIterator(() => getIterator(fileInfo))
        val intShape = builder.add(indexer)

        SourceShape(intShape.out)
    }
  }
}

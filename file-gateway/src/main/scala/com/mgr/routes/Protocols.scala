package com.mgr.routes

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

case class UserData(username: String)

case class Credentials(username: String, password: String)

case class FileRequest(fileName: String)

object Protocols extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val userDataFormat: RootJsonFormat[UserData] = jsonFormat1(UserData)
  implicit val credentialsFormat: RootJsonFormat[Credentials] = jsonFormat2(Credentials)
  implicit val fileRequestFormat: RootJsonFormat[FileRequest] = jsonFormat1(FileRequest)
}

package com.mgr

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import com.hazelcast.Scala._
import com.hazelcast.config.Config
import com.mgr.actors.FileInfoRepository
import com.mgr.grid._
import com.mgr.routes.FileRoutes
import com.mgr.util.StorageType
import com.typesafe.config.ConfigFactory

import scala.concurrent.ExecutionContextExecutor

object GatewayMicroservice extends App
  with FileRoutes {

  private val defaultConfig = ConfigFactory.load()
  println(defaultConfig.getString("akka.deploy").toUpperCase)

  private val hzConfig = new Config
  private val hzInstance = hzConfig.newInstance()
  implicit val chunkMap = hzInstance.getMap[FileChunkKey, FileChunk]("chunkMap")

  implicit val fileInfoMap = hzInstance.getMap[Long, FileInfo]("fileInfoMap")
  implicit val fileInfoIdGenerator = hzInstance.getIdGenerator("fileInfoIdGenerator")

  implicit val system: ActorSystem = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  //  val chunksRepository: ChunkRepository = TypedActor(system).typedActorOf(ChunkRepository.props(), "chunksRepository")
  val fileInfoRepository = system.actorOf(FileInfoRepository.props(fileInfoMap, fileInfoIdGenerator), "fileInfoRepository")

  val routes = StorageType.withName(defaultConfig.getString("storage-type")) match {
    case StorageType.hz =>
      pathPrefix("api") {
        fileRoutes(fileInfoRepository)
      }
    case StorageType.file =>
      pathPrefix("api") {
        fileFsRoutes()
      }
  }

    Http().bindAndHandle(routes, "localhost", 8888)
  //  system.registerOnTermination(closeCaches())
}




package com.mgr.exceptions

import akka.http.scaladsl.model.{StatusCode, StatusCodes}

trait HttpUserException {
  def statusCode: StatusCode

  def message: String
}

case class FileInfoExists(statusCode: StatusCode = StatusCodes.BadRequest,
                          message: String = "File info already exists") extends HttpUserException

case class FileInfoNotFound(statusCode: StatusCode = StatusCodes.NotFound,
                            message: String = "File was not found") extends HttpUserException

case class ChunkExistException(message: String = "Chunk already exists in data grid")
  extends RuntimeException(message)

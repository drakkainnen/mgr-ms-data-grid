enablePlugins(JavaAppPackaging)

name := "file-gateway"
version := "1.0"
scalaVersion := "2.11.8"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

resolvers += Resolver.jcenterRepo

mainClass in Compile := Some("com.mgr.GatewayMicroservice")

libraryDependencies ++= {
  val akkaV = "2.5.2"
  val akkaHttpV = "10.0.7"
  val scalaTestV = "3.0.1"
  val betterFilesV = "3.0.0"

  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaV,
    "com.typesafe.akka" %% "akka-stream" % akkaV,
    "com.typesafe.akka" %% "akka-http" % akkaHttpV,
    "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpV,

//    "com.github.pathikrit" %% "better-files" % betterFilesV,

    "com.hazelcast" % "hazelcast" % "3.8.1",
    "com.hazelcast" % "hazelcast-client" % "3.8.1",
    "com.hazelcast" %% "hazelcast-scala" % "3.7.2",
    "com.hazelcast.jet" % "hazelcast-jet" % "0.3.1",

    "org.scalatest" %% "scalatest" % scalaTestV % "test",
  "com.typesafe.akka" %% "akka-testkit" % akkaV % "test",
  "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpV % "test"

    //    "io.kamon" %% "kamon-core" % "0.6.6",
    //    "io.kamon" %% "kamon-akka-2.4" % "0.6.6",
    //    "io.kamon" %% "kamon-akka-http" % "0.6.6"
  )
}
Revolver.settings

//Required by kamon
//aspectjSettings
//
//javaOptions in run <++= AspectjKeys.weaverOptions in Aspectj
//
//fork in run := true



import { GatewayPage } from './app.po';

describe('gateway App', () => {
  let page: GatewayPage;

  beforeEach(() => {
    page = new GatewayPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadComponent } from './upload.component';
import {UploadRoutesModule} from "./upload-routes.module";
import {FileUploadModule} from "ng2-file-upload";

@NgModule({
  imports: [
    CommonModule,
    FileUploadModule,
    UploadRoutesModule
  ],
  declarations: [UploadComponent]
})
export class UploadModule { }

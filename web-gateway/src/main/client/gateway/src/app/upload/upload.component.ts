import {Component, OnInit} from '@angular/core';
import {Headers, Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/catch";
import {FileUploader} from "ng2-file-upload";

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.less']
})
export class UploadComponent implements OnInit {
  public uploader:FileUploader = new FileUploader({url: '/api/file/upload2'});

  constructor(private http: Http) {
  }

  ngOnInit() {
  }

  fileChange(event) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append('file', file, file.name);

      let headers = new Headers();
      // headers.append('Content-Type', 'multipart/form-data');
      headers.append('Accept', '*/*');

      let options = new RequestOptions({headers: headers});

      this.http.post('/api/file/upload2', formData, options)
        .map(res => res.json())
        .catch(error => Observable.throw(error))
        .subscribe(
          error => console.log(error),
          () => console.log('success')
        )
    }
  }
}

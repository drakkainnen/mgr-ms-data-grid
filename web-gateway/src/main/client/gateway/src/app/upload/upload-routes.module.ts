import {Routes, RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {UploadComponent} from "./upload.component";

const uploadRoutes: Routes = [
  {path: 'upload', component: UploadComponent}
];


@NgModule({
  imports: [
    RouterModule.forChild(uploadRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class UploadRoutesModule {
}


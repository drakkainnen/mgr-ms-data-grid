import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilesComponent } from './files.component';
import {FilesRoutesModule} from "./files-routes.module";

@NgModule({
  imports: [
    CommonModule,
    FilesRoutesModule
  ],
  declarations: [FilesComponent]
})
export class FilesModule { }

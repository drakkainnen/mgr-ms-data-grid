import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {FilesComponent} from "./files.component";

const filesRoutes: Routes = [
  {path: 'files', component: FilesComponent}
];

@NgModule({
  imports: [
    RouterModule.forChild(filesRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class FilesRoutesModule {
}

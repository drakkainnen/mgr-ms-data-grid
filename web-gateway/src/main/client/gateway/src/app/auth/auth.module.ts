import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AuthRoutingModule} from "./auth-routing.module";
import {AuthService} from "./services/auth.service";
import {LogInComponent} from "./login/loging.component";
import {HttpModule} from "@angular/http";
import {BrowserModule} from "@angular/platform-browser";
import {LogoutComponent} from './logout/logout.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { UserService } from './services/user.service';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AuthRoutingModule
  ],
  providers: [
    AuthService,
    UserService
  ],
  declarations: [
    LogInComponent,
    LogoutComponent,
    SignInComponent
  ]
})
export class AuthModule {
}

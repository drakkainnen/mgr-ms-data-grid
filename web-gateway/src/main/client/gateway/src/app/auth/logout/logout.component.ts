import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.less']
})
export class LogoutComponent implements OnInit {

  message:string;

  constructor(private router: Router, private authService: AuthService) {
  }

  ngOnInit() {
    this.authService.logout()
      .subscribe({
        error: error => this.handleErrors(error),
        complete: () => this.showOkAndRedirect()
      });
  }

  private showOkAndRedirect() {
    this.message = 'Successfully logged out!';
    setTimeout(() => this.router.navigate(['/home']), 1500);
  }

  private handleErrors(error) {
    if(error.status == 403) {
      this.message = 'You were already logged out';
    }
  }
}

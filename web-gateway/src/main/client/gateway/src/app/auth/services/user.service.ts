import {Injectable} from '@angular/core';
import {Http} from "@angular/http";
import {UserData} from "../../classes/user-data";
import {Observable} from "rxjs/Observable";

@Injectable()
export class UserService {

  constructor(private http: Http) {
  }

  createUser(userData: UserData): Observable<boolean> {
    console.log(userData);
    return this.http.post('/api/user', userData)
      .map(r => r.ok);
  }
}

import {Component, OnInit} from "@angular/core";
import {Credentials} from "../../classes/credentials";
import {AuthService} from "../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LogInComponent implements OnInit {

  credentials: Credentials = new Credentials();

  constructor(private authRestService: AuthService,
              private router: Router) {
  }

  ngOnInit() {
  }

  signIn() {
    console.log(this.credentials);
    this.authRestService.login(this.credentials)
      .subscribe({
        complete: () => this.redirectToHome(),
        error: (err: any) => console.log(err)
      });
  }

  private redirectToHome() {
    this.router.navigate(['/home'])
      .catch(err => console.log(err));
  }
}

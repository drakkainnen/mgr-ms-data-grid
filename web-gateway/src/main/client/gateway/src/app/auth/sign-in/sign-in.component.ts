import {Component, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl} from "@angular/forms";
import {UserService} from "../services/user.service";
import {ValidateFn} from "codelyzer/walkerFactory/walkerFn";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.less']
})
export class SignInComponent implements OnInit {

  userDataForm: FormGroup;

  constructor(private fb: FormBuilder, private userService: UserService) {
    this.createForm()
  }

  ngOnInit() {
  }

  submitForm() {
    console.log('oho!');
    let userData = this.userDataForm.value;
    console.log(userData);
    this.userService.createUser(userData);
  }

  private createForm() {
    this.userDataForm = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(2)]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      password: ['', [Validators.required]],
      passwordAgain: ['', [Validators.required]]
    }, {validator: this.fieldsMatching('password', 'passwordAgain')});
  }

  fieldsMatching(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      let no = password.value !== confirmPassword.value;
      return no ? {fieldsMatching: true} : null;
    }
  }
}

import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {LogInComponent} from "./login/loging.component";
import {LogoutComponent} from "./logout/logout.component";
import {SignInComponent} from "./sign-in/sign-in.component";

const authRoutes: Routes = [
  {
    path: 'login',
    component: LogInComponent
  },
  {
    path: 'logout',
    component: LogoutComponent
  },
  {
    path: 'sign-in',
    component: SignInComponent
  }];

@NgModule({
  imports: [
    RouterModule.forChild(authRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AuthRoutingModule {
}

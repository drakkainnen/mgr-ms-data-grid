import {Headers, Http, Response} from "@angular/http";
import {Injectable} from "@angular/core";
import {Credentials} from "../../classes/credentials";
import {Observable} from "rxjs/Observable";

@Injectable()
export class AuthService {
  constructor(private http: Http) {
  }

  login(credentials: Credentials): Observable<Response> {
    return this.http.get('/api/login', {headers: this.prepareHeaders(credentials)});
  }

  logout(): Observable<Response> {
    return this.http.get('/api/logout');
  }

  private prepareHeaders(credentials: Credentials) {
    let headers = new Headers();
    let cryptedCredentials = btoa(credentials.username + ':' + credentials.password);
    headers.set('Authorization', 'Basic ' + cryptedCredentials + '==');
    return headers;
  }
}

package com.mgr.routes

import akka.actor.ActorRef
import akka.pattern._
import akka.http.scaladsl.server.directives.Credentials
import akka.util.Timeout

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._

/**
  * Created by BBUDNIK on 30.05.2017.
  */
trait AuthClient {

  implicit val executionContext: ExecutionContext
  implicit val localActor: ActorRef
  implicit val timeout: Timeout = Timeout(2.seconds)

//  def authenticate(cred: Credentials): Future[Option[Long]] = {
////    val ask = localActor.ask(cred)(timeout, null)
//////    val ask = ask(localActor, cred)
////    ask.map {
////      case Some(d: Long) =>
////        Some(d)
////    }
//    //    Future.successful(Some(1))
//  }

  //  lazy val authConnectionFlow = Http().outgoingConnection("localhost", port = 8888)
  //
  //  def authRequest(request: HttpRequest): Future[HttpResponse] = {
  //    Source.single(request).via(authConnectionFlow).runWith(Sink.head)
  //  }
  //
  //  def checkCredentials(credentials: Credentials): Future[Long] = {
  //  }
  //
  //  private def sendCredsAndMapResult(creds: Credentials) = {
  ////    val request = authRequest(RequestBuilding.Get("/api/remote").addCredentials(creds))
  ////    request.flatMap { response =>
  ////      response.status match {
  ////        case StatusCodes.OK =>
  ////          Unmarshal(response.entity).to[String].map(_.toLong)
  ////        case _ =>
  ////          Future.failed(new Exception)
  ////      }
  ////    }
  //  }

}

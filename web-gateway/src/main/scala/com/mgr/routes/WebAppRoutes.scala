package com.mgr.routes

import akka.pattern._
import akka.actor.ActorRef
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.directives.Credentials
import akka.util.Timeout

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

trait WebAppRoutes {

  def webappResources = {
    pathEndOrSingleSlash {
      getFromResource("webapp/index.html")
    } ~ getFromResourceDirectory("webapp")
  }
}

trait AuthRoutes {
  implicit val localAuthActor: ActorRef
  def authRoutes = {
    implicit val timeout = Timeout(2.seconds)
    pathPrefix("api") {
      path("login") {
        authenticateBasicAsync("secured", (c: Credentials) => {
          val future = localAuthActor ask c
          future.mapTo[Option[Long]]
        }) { data =>
          complete("ok")
        }
      }
    }
  }

}



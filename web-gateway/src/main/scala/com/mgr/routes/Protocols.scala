package com.mgr.routes

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

/**
  * Created by BBUDNIK on 29.05.2017.
  */
trait Protocols extends DefaultJsonProtocol with SprayJsonSupport {

}

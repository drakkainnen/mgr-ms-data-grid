package com.mgr.util

//import akka.actor.ActorRef
//import akka.util.Timeout
//import com.softwaremill.session.{RefreshTokenData, RefreshTokenLookupResult, RefreshTokenStorage}
//
//import scala.concurrent.{ExecutionContextExecutor, Future}
//import scala.concurrent.duration.Duration

//class HzRefreshTokenStorage[T](hz: HazelcastInstance) extends RefreshTokenStorage[T] {
//  private val _store = hz.getMap[String, UserSession[T]]("sessionCache")
//
//  override def lookup(selector: String): Future[Option[RefreshTokenLookupResult[T]]] = {
//    Future.successful {
//      Option(_store.get(selector))
//        .map(s => RefreshTokenLookupResult[T](s.tokenHash, s.expires, () => s.session))
//    }
//  }
//
//  override def store(data: RefreshTokenData[T]): Future[Unit] = {
//    Future.successful(_store.put(data.selector, UserSession(data.forSession, data.tokenHash, data.expires)))
//  }
//
//  override def remove(selector: String): Future[Unit] = {
//    Future.successful(_store.remove(selector))
//  }
//
//  override def schedule[S](after: Duration)(op: => Future[S]): Unit = {
//    op
//    Future.successful(())
//  }
//}

//class ActorRefreshTokenStorage[T](actor: ActorRef)(implicit executionContext: ExecutionContextExecutor) extends RefreshTokenStorage[T] {
//
//  import akka.pattern._
//
//  import scala.concurrent.duration._
//
//  implicit val timeout = Timeout(1.second)
//
//  override def lookup(selector: String): Future[Option[RefreshTokenLookupResult[T]]] = {
//    actor ? FindSession(selector) map {
//      case Some(s: UserSession[T]) =>
//        //      case Option(s:UserSession[T]) =>
//        Some(RefreshTokenLookupResult[T](s.tokenHash, s.expires, () => s.session))
//      case _ =>
//        None
//    }
//  }
//
//  override def store(data: RefreshTokenData[T]): Future[Unit] = {
//    val sessionEvent = StoreSession[T](data.selector, UserSession(data.forSession, data.tokenHash, data.expires))
//    actor ? sessionEvent map (_ => ())
//  }
//
//  override def remove(selector: String): Future[Unit] = {
//    actor ? RemoveSession(selector) map (_ => ())
//  }
//
//  override def schedule[S](after: Duration)(op: => Future[S]): Unit = {
//    op
//  }
//}


package com.mgr

import akka.actor.Actor.Receive
import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.directives.Credentials
import akka.routing.RoundRobinGroup
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.mgr.routes.{AuthClient, AuthRoutes, WebAppRoutes}
import com.typesafe.config.{Config, ConfigFactory}

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.collection.JavaConverters._

object WebGateway
  extends App
    with WebAppRoutes
    with AuthRoutes {
  val config: Config = ConfigFactory.load()

  implicit val system: ActorSystem = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher


  private val authActorRoutes = config.getStringList("auth-ms.routes").asScala.toList
  private val fileActorRoutes = config.getStringList("file-ms.routes").asScala.toList

  implicit val localAuthActor = system.actorOf(LocalActor.authProps(authActorRoutes, "authActor"), "localAuthActor")

  implicit val localFileActor = system.actorOf(LocalActor.fileProps(fileActorRoutes, "fileActor"), "localFileActor")

  //  implicit val localactor = system.actorOf(RoundRobinGroup(toList).props(), "authActors")

  //  private val sessionConfig = SessionConfig.default("12345678912345678912345678912345678912345678912345678912345678912345678912345678912345678900000000001234567890")
  //  implicit val sessionManager = new SessionManager[Long](sessionConfig)
  //  implicit val refreshTokenStorage = new ActorRefreshTokenStorage[Long](hzSupervisor)


  //  lazy val authConnectionFlow = Http().outgoingConnection("localhost", port = 8888)
  //
  //  def authRequest(request: HttpRequest): Future[HttpResponse] = {
  //    Source.single(request).via(authConnectionFlow).runWith(Sink.head)
  //  }
  //
  //  def checkCredentials(credentials: HttpCredentials): Future[Long] = {
  //    val request = authRequest(RequestBuilding.Get("/api/login").addCredentials(credentials))
  //    request.flatMap { response =>
  //      response.status match {
  //        case StatusCodes.OK =>
  //          Unmarshal(response.entity).to[Long]
  //        case _ =>
  //          Future.failed(new Exception)
  //      }
  //    }
  //  }

  //  selection.tell("makumba", localActor)

  Http().bindAndHandle(webappResources ~ authRoutes, config.getString("http.host"), config.getInt("http.port"))
}

object LocalActor {
  def authProps(urlList: List[String], actorName: String): Props =
    Props(new LocalActor(urlList, actorName) with AuthRemoteBehavior)

  def fileProps(urlList: List[String], actorName: String): Props =
    Props(new LocalActor(urlList, actorName) with FileRemoteBehavior)
}

class LocalActor(urlList: List[String], actorName: String) extends Actor with ActorLogging {
  beh: RemoteBehavior =>
  implicit val timeout = Timeout(2.seconds)
  implicit val ec = context.dispatcher

  lazy val remote = context.actorOf(RoundRobinGroup(urlList).props(), actorName)

  override def getRemote(): ActorRef = {
    remote
  }

  override def receive: Receive = beh.behavior
}

trait RemoteBehavior {
  def getRemote(): ActorRef

  val behavior: Receive
}

trait AuthRemoteBehavior extends RemoteBehavior {
  this: Actor
    with ActorLogging =>

  override val behavior: Receive = {
    case c@Credentials.Provided(_) =>
      getRemote().forward(c)
    case c@Credentials.Missing =>
      sender() ! None
    case o =>
      getRemote().forward(o)
  }

}

trait FileRemoteBehavior extends RemoteBehavior {
  this: Actor
    with ActorLogging =>

  override val behavior: Receive = {
    case o =>
      getRemote().forward(o)
  }
}

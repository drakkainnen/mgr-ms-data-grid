enablePlugins(JavaAppPackaging)

name := "file-management"
version := "1.0"
scalaVersion := "2.11.8"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

resolvers ++= Seq(Resolver.jcenterRepo, Resolver.bintrayRepo("clockfly", "maven"))

libraryDependencies ++= {
  val akkaV = "2.5.2"
  val akkaHttpV = "10.0.7"
  val scalaTestV = "3.0.1"
  val hazelcastV = "3.8.1"
  val hazelcastScalaV = "3.7.2"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaV,
    "com.typesafe.akka" %% "akka-stream" % akkaV,
    "com.typesafe.akka" %% "akka-remote" % akkaV,
    "com.typesafe.akka" %% "akka-http" % akkaHttpV,
    "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpV,

    "com.hazelcast" % "hazelcast" % hazelcastV,
    "com.hazelcast" % "hazelcast-client" % hazelcastV,
    "com.hazelcast" %% "hazelcast-scala" % hazelcastScalaV,

    "org.scalatest" %% "scalatest" % scalaTestV % "test",
    "com.typesafe.akka" %% "akka-testkit" % akkaV % "test",
    "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpV % "test"
  )
}
Revolver.settings

//Required by kamon
//aspectjSettings
//
//javaOptions in run <++= AspectjKeys.weaverOptions in Aspectj
//
//fork in run := true



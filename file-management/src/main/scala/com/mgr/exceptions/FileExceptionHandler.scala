package com.mgr.exceptions

import akka.http.scaladsl.model.{StatusCode, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.ExceptionHandler
import com.mgr.exceptions.FileExceptionHandler.{FileInfoExists, FileInfoNotFound}

object FileExceptionHandler {

  case class FileInfoExists(statusCode: StatusCode = StatusCodes.BadRequest,
                            message: String = "File info already exists") extends RuntimeException(message)

  case class FileInfoNotFound(statusCode: StatusCode = StatusCodes.NotFound,
                              message: String = "File was not found") extends RuntimeException(message)

}

trait FileExceptionHandler {

  def createExceptionHandler: ExceptionHandler = ExceptionHandler {
    case e: FileInfoNotFound =>
      complete(e.statusCode, e.getMessage())
    case e: FileInfoExists =>
      complete(e.statusCode, e.getMessage())
    case e: Throwable =>
      failWith(e)
  }
}


package com.mgr.actors

import akka.actor.Status.Failure
import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.stream.ActorMaterializer
import com.mgr.actors.FileInfoRepositoryHandlerComponent.{CreateFileInfo, GetFile, ReplaceFileInfo}
import com.mgr.exceptions.FileExceptionHandler.{FileInfoExists, FileInfoNotFound}
import com.mgr.grid.Entities.FileInfo

import scala.concurrent.ExecutionContextExecutor

trait FileInfoRepositoryComponent {
  val fileInfoManager: FileInfoRepository

  trait FileInfoRepository {

    def getById(userId: Long, id: Long): Option[FileInfo]

    def getByName(userId: Long, fileName: String): Option[FileInfo]

    def create(fileName: String): Option[FileInfo]

    def replace(current: FileInfo, newValue: FileInfo)
  }

}

//trait FileInfoHazelcast {
//  implicit val fileInfoMap: IMap[Long, FileInfo]
//  implicit val fileInfoIdGen: IdGenerator
//
//  def getById(userId: Long, id: Long): Option[FileInfo] = {
//    val fileInfo = fileInfoMap.get(id)
//    fileInfo match {
//      case f: FileInfo if f.userId == userId =>
//        return Some(f)
//      case _ =>
//        return None
//    }
//  }
//
//  def getByName(userId: Long, fileName: String): Option[FileInfo] = {
//    val predicate = new PredicateBuilder().getEntryObject()
//      .get("userId").equal(userId)
//      .and(new PredicateBuilder().getEntryObject().get("fileName").equal(fileName))
//    val values = fileInfoMap.values(predicate).asScala
//    values.headOption
//  }
//
//  def create(fileName: String): Option[FileInfo] = {
//    val fileInfo = FileInfo(fileInfoIdGen.newId(), 0, fileName, -1, LocalDate.now())
//    val absent = fileInfoMap.putIfAbsent(fileInfo.id, fileInfo)
//
//    if (absent == null) {
//      return Some(fileInfo)
//    }
//    return None
//  }
//
//  def replace(current: FileInfo, newValue: FileInfo) = {
//    fileInfoMap.replace(current.id, current, newValue)
//  }
//}

object FileInfoRepositoryHandlerComponent {

  case class GetAll(userId: Long)

  case class GetFile(userId: Long, fileName: Option[String], id: Option[Long])

  case class ReplaceFileInfo(fileName: FileInfo, numberOfChunks: Long)

  case class CreateFileInfo(fileName: String)

}

trait FileInfoRepositoryHandlerComponent {
  this: FileInfoRepositoryComponent =>

  def fileInfoRepositoryProps(): Props = Props(new FileInfoRepositoryHandler)

  class FileInfoRepositoryHandler extends Actor with ActorLogging {
    implicit val system: ActorSystem = context.system
    implicit val executionContext: ExecutionContextExecutor = context.dispatcher
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    log.debug(system.dispatcher.toString)

    override def receive: Receive = {
      case GetFile(userId, Some(fileName), _) =>
        fileInfoManager.getByName(userId, fileName) match {
          case Some(fileInfo) =>
            sender() ! fileInfo
          case _ =>
            sender() ! Failure(FileInfoNotFound())
        }

      case GetFile(userId, _, Some(id)) =>
        fileInfoManager.getById(userId, id) match {
          case Some(fileInfo) =>
            sender() ! fileInfo
          case _ =>
            sender() ! Failure(FileInfoNotFound())
        }

      case CreateFileInfo(fileName) =>
        fileInfoManager.create(fileName) match {
          case Some(fileInfo) =>
            sender() ! fileInfo
          case None =>
            sender() ! Failure(FileInfoExists())
        }

      case ReplaceFileInfo(fileInfo, numberOfChunks) =>
        val updatedInfo = fileInfo.copy(numberOfChunks = numberOfChunks)
        fileInfoManager.replace(fileInfo, updatedInfo)

      case _ =>
        sender() ! Failure(FileInfoNotFound())
    }
  }

}

//class FileInfoRepository
//  extends Actor with ActorLogging {
//
//  implicit val system: ActorSystem = context.system
//  implicit val executionContext: ExecutionContextExecutor = context.dispatcher
//  implicit val materializer: ActorMaterializer = ActorMaterializer()
//
//  log.debug(system.dispatcher.toString)
//
//  override def receive: Receive = {
//    case GetFile(userId, Some(fileName), _) =>
//      getByName(userId, fileName) match {
//        case Some(fileInfo) =>
//          sender() ! fileInfo
//        case _ =>
//          sender() ! Failure(FileInfoNotFound())
//      }
//
//    case GetFile(userId, _, Some(id)) =>
//      getById(userId, id) match {
//        case Some(fileInfo) =>
//          sender() ! fileInfo
//        case _ =>
//          sender() ! Failure(FileInfoNotFound())
//      }
//
//    case CreateFileInfo(fileName) =>
//      create(fileName) match {
//        case Some(fileInfo) =>
//          sender() ! fileInfo
//        case None =>
//          sender() ! Failure(FileInfoExists())
//      }
//
//    case ReplaceFileInfo(fileInfo, numberOfChunks) =>
//      val updatedInfo = fileInfo.copy(numberOfChunks = numberOfChunks)
//      replace(fileInfo, updatedInfo)
//
//    case _ =>
//      sender() ! Failure(FileInfoNotFound())
//  }
//}
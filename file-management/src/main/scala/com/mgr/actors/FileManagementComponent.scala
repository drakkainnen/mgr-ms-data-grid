package com.mgr.actors

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import akka.stream.ActorMaterializer

trait FileManagementComponent {
  val fileManager: FileManager

  trait FileManager {
    def compress(fileInfoId: Long): Unit

    def decompress(fileInfoId: Long): Unit

    def delete(fileInfoId: Long): Unit
  }

}

object FileManagementHandlerComponent {

  case class CompressData(fileInfoId: Long)

  case class DecompressData(fileInfoId: Long)

  case class Delete(fileInfoId: Long)

}

trait FileManagementHandlerComponent {
  this: FileManagementComponent =>

  def fileManagerProps(): Props = Props(new FileManagementHandler)

  class FileManagementHandler extends Actor with ActorLogging {

    import com.mgr.actors.FileManagementHandlerComponent._

    implicit val system: ActorSystem = context.system
    implicit val executionContext = system.dispatcher
    implicit val materializer = ActorMaterializer()

    log.debug(context.dispatcher.toString)

    override def receive: Receive = {
      case CompressData(fi) =>
        fileManager.compress(fi)
      case DecompressData(fi) =>
        fileManager.decompress(fi)
      case Delete(fi) =>
        fileManager.delete(fi)
    }
  }

}



package com.mgr.actors

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.mgr.grid._
import com.mgr.actors.FileInfoRepositoryHandlerComponent.{GetAll, GetFile}
import com.mgr.actors.FileManagementHandlerComponent.{CompressData, DecompressData, Delete}
import akka.actor.AllForOneStrategy
import akka.actor.SupervisorStrategy._

import scala.concurrent.duration._

object ActorSupervisor {
  def clientProps: Props = Props(new ActorSupervisor()
    with HzClientComponent
    with HzFileInfoRepositoryComponent
    with HzFileManagementComponent
    with FileInfoRepositoryHandlerComponent
    with FileManagementHandlerComponent)

  def serverProps: Props = Props(new ActorSupervisor()
    with HzServerComponent
    with HzFileInfoRepositoryComponent
    with HzFileManagementComponent
    with FileInfoRepositoryHandlerComponent
    with FileManagementHandlerComponent)
}

class ActorSupervisor extends Actor with ActorLogging {
  this: FileInfoRepositoryHandlerComponent
    with FileManagementHandlerComponent
    with FileManagementComponent
    with FileInfoRepositoryComponent
    with HzComponent =>

  override val supervisorStrategy = AllForOneStrategy(maxNrOfRetries = 10, withinTimeRange = 1.minute, loggingEnabled = true) {
    case _: IllegalStateException => Resume
    case _: NullPointerException => Resume
    case _: Exception => Escalate
  }

  val fileManagementHandler: ActorRef = context.actorOf(fileManagerProps(), "fileManagementHandler")
  val fileInfoRepository: ActorRef = context.actorOf(fileInfoRepositoryProps(), "fileInfoRepositoryHandler")

  log.info(context.dispatcher.toString)

  override def receive: Receive = {
    case m: CompressData =>
      fileManagementHandler.forward(m)
    case m: DecompressData =>
      fileManagementHandler.forward(m)
    case m: Delete =>
      fileManagementHandler.forward(m)

    case m: GetAll =>
      fileInfoRepository.forward(m)
    case m: GetFile =>
      fileInfoRepository.forward(m)
  }
}


package com.mgr.routes

import akka.actor.ActorRef
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.pattern.ask
import akka.util.Timeout
import com.mgr.actors.FileInfoRepositoryHandlerComponent.{GetAll, GetFile}
import com.mgr.actors.FileManagementHandlerComponent.{CompressData, DecompressData, Delete}
import com.mgr.exceptions.FileExceptionHandler
import com.mgr.grid.Entities.FileInfo

trait FileManagementRoutes extends Protocols with FileExceptionHandler {
  implicit val supervisorActor: ActorRef
  implicit val timeout: Timeout

  def fileManagement(): Route = {

    handleExceptions(createExceptionHandler) {
      pathPrefix("api") {
        manageSection ~ findSection
      }
    }
  }

  private def manageSection = {
    path("user" / LongNumber / "file-info" / LongNumber) { (userId, fileInfoId) =>
      delete {
        supervisorActor ! Delete(fileInfoId)
        complete(StatusCodes.OK)
      }
    } ~ path("user" / LongNumber / "file-info" / LongNumber / "compress") { (userId, fileInfoId) =>
      supervisorActor ! CompressData(fileInfoId)
      complete(StatusCodes.OK)
    } ~ path("user" / LongNumber / "file-info" / LongNumber / "decompress") { (userId, fileInfoId) =>
      supervisorActor ! DecompressData(fileInfoId)
      complete(StatusCodes.OK)
    }
  }

  private def findSection = {
    path("user" / LongNumber / "file-info") { userId =>
      get {
        onSuccess(supervisorActor ? GetAll(userId)) {
          case d: Seq[FileInfo] =>
            complete(d)
        }
      }
    } ~ path("user" / LongNumber / "file-info" / LongNumber) { (userId, fileId) =>
      get {
        onSuccess(supervisorActor ? GetFile(userId, None, Some(fileId))) {
          case fi: FileInfo =>
            complete(fi)
        }
      }
    } ~ path("user" / LongNumber / "file-info" / Segment) { (userId, name) =>
      onSuccess(supervisorActor ? GetFile(userId, Some(name), None)) {
        case fi: FileInfo =>
          complete(fi)
      }
    }
  }

}

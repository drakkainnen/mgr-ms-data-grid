package com.mgr.routes

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.mgr.grid.Entities.FileInfo
import spray.json.{DefaultJsonProtocol, DeserializationException, JsString, JsValue, JsonFormat, RootJsonFormat}


trait Protocols extends DefaultJsonProtocol with SprayJsonSupport {
  //  implicit val userDataFormat: RootJsonFormat[UserData] = jsonFormat4(UserData)

  implicit object LocalDateFormat extends JsonFormat[LocalDate] {
    override def read(json: JsValue): LocalDate = json match {
      case JsString(s) => LocalDate.parse(s, DateTimeFormatter.ISO_DATE)
      case x => throw new DeserializationException("Expected LocalDate as JsString, but got " + x)
    }

    override def write(obj: LocalDate): JsValue = {
      JsString(obj.format(DateTimeFormatter.ISO_DATE))
    }
  }

  implicit val fileInfoFormat: RootJsonFormat[FileInfo] = jsonFormat5(FileInfo)

}

package com.mgr.grid

import java.util.Map

import com.hazelcast.map.{EntryBackupProcessor, EntryProcessor}
import com.hazelcast.nio.IOUtil
import com.mgr.grid.Entities.{FileChunk, FileChunkKey}

class CompressEntryProcessor
  extends EntryProcessor[FileChunkKey, FileChunk]
    with EntryBackupProcessor[FileChunkKey, FileChunk]
    with Serializable {

  override def process(entry: Map.Entry[FileChunkKey, FileChunk]): Seq[Byte] = {
    val value = entry.getValue()
    val compress = IOUtil.compress(value.bytes.toArray)
    val copy = value.copy(bytes = compress)

    entry.setValue(copy)
    compress.toSeq
  }

  override def getBackupProcessor: EntryBackupProcessor[FileChunkKey, FileChunk] = {
    CompressEntryProcessor.this
  }

  override def processBackup(entry: Map.Entry[FileChunkKey, FileChunk]): Unit = {
    val value = entry.getValue
    val compress = IOUtil.compress(value.bytes.toArray)
    val copy = value.copy(bytes = compress.toSeq)

    entry.setValue(copy)
  }
}

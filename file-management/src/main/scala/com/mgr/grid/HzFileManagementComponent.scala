package com.mgr.grid

import com.hazelcast.core.{HazelcastInstance, IMap}
import com.hazelcast.query.{Predicate, Predicates}
import com.mgr.actors.FileManagementComponent
import com.mgr.grid.Entities.{FileChunk, FileChunkKey}

trait HzFileManagementComponent extends FileManagementComponent {
  this: HzComponent =>
  override val fileManager = new HzFileManager(hzInstance)

  class HzFileManager(hzInstance: HazelcastInstance) extends FileManager {

    private val chunkMap: IMap[FileChunkKey, FileChunk] = hzInstance.getMap(HzFileManagementComponent.chunkMap)

    override def compress(fileInfoId: Long): Unit = {
      val filePredicate = Predicates.equal("fileInfoId", fileInfoId)
      chunkMap.executeOnEntries(new CompressEntryProcessor, filePredicate)
    }

    override def decompress(fileInfoId: Long): Unit = {
      val filePredicate = Predicates.equal("fileInfoId", fileInfoId)
      chunkMap.executeOnEntries(new DecompressEntryProcesor, filePredicate)
    }

    override def delete(fileInfoId: Long): Unit = {
      val equal = Predicates.equal("fileInfoId", fileInfoId)
      chunkMap.removeAll(equal.asInstanceOf[Predicate[FileChunkKey, FileChunk]])
    }
  }

}

object HzFileManagementComponent {
  val chunkMap: String = "chunkMap"
}

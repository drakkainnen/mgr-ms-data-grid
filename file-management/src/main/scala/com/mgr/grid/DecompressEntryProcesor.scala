package com.mgr.grid

import java.util.Map

import com.hazelcast.map.{EntryBackupProcessor, EntryProcessor}
import com.hazelcast.nio.IOUtil
import com.mgr.grid.Entities.{FileChunk, FileChunkKey}

class DecompressEntryProcesor
  extends EntryProcessor[FileChunkKey, FileChunk]
    with EntryBackupProcessor[FileChunkKey, FileChunk] {

  override def process(entry: Map.Entry[FileChunkKey, FileChunk]): Seq[Byte] = {
    val value = entry.getValue()
    val decompress = IOUtil.decompress(value.bytes.toArray)
    val copy = value.copy(bytes = decompress)

    entry.setValue(copy)
    decompress
  }

  override def getBackupProcessor: EntryBackupProcessor[FileChunkKey, FileChunk] = {
    DecompressEntryProcesor.this
  }

  override def processBackup(entry: Map.Entry[FileChunkKey, FileChunk]): Unit = {
    val value = entry.getValue
    val decompress = IOUtil.decompress(value.bytes.toArray)
    val copy = value.copy(bytes = decompress)

    entry.setValue(copy)
  }
}

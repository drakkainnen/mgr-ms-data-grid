package com.mgr.grid

import java.time.LocalDate

import com.hazelcast.core.HazelcastInstance
import com.hazelcast.query.PredicateBuilder
import com.mgr.actors.FileInfoRepositoryComponent
import com.mgr.grid.Entities.FileInfo

import scala.collection.JavaConverters._

trait HzFileInfoRepositoryComponent extends FileInfoRepositoryComponent {
  this: HzComponent =>
  override val fileInfoManager = new HzFileInfoRepository(hzInstance)

  class HzFileInfoRepository(hzInstance: HazelcastInstance) extends FileInfoRepository {
    val fileInfoMap = hzInstance.getMap[Long, FileInfo](HzFileInfoRepositoryComponent.cacheName)
    val fileInfoIdGen = hzInstance.getIdGenerator(HzFileInfoRepositoryComponent.generatorName)

    def getById(userId: Long, id: Long): Option[FileInfo] = {
      val fileInfo = fileInfoMap.get(id)
      fileInfo match {
        case f: FileInfo if f.userId == userId =>
          return Some(f)
        case _ =>
          return None
      }
    }

    def getByName(userId: Long, fileName: String): Option[FileInfo] = {
      val predicate = new PredicateBuilder().getEntryObject()
        .get("userId").equal(userId)
        .and(new PredicateBuilder().getEntryObject().get("fileName").equal(fileName))
      val values = fileInfoMap.values(predicate).asScala
      values.headOption
    }

    def create(fileName: String): Option[FileInfo] = {
      val fileInfo = FileInfo(fileInfoIdGen.newId(), 0, fileName, -1, LocalDate.now())
      val absent = fileInfoMap.putIfAbsent(fileInfo.id, fileInfo)

      if (absent == null) {
        return Some(fileInfo)
      }
      return None
    }

    def replace(current: FileInfo, newValue: FileInfo) = {
      fileInfoMap.replace(current.id, current, newValue)
    }
  }

}

object HzFileInfoRepositoryComponent {
  val cacheName = "fileInfo"
  val generatorName = "fileInfoGenerator"
}


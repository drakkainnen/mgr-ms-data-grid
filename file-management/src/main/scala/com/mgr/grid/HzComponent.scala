package com.mgr.grid

import com.hazelcast.Scala._
import com.hazelcast.core.HazelcastInstance

trait HzComponent {
  val hzInstance: HazelcastInstance
}

trait HzServerComponent extends HzComponent {

  import com.hazelcast.config.Config

  private val hzConfig = new Config()
  override val hzInstance = hzConfig.newInstance()
}

trait HzClientComponent extends HzComponent {

  import com.hazelcast.Scala.client._
  import com.hazelcast.client.config.ClientConfig

  private val hzConfig = new ClientConfig()
  override val hzInstance = hzConfig.newClient()
}

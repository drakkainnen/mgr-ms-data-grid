package com.mgr

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.pattern.{Backoff, BackoffSupervisor}
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.mgr.actors.ActorSupervisor
import com.mgr.routes.FileManagementRoutes

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._

object FileManagementMicroservice
  extends App with FileManagementRoutes {

  implicit val system: ActorSystem = ActorSystem("file-management")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  override implicit val timeout: Timeout = Timeout(1.second)
  override implicit val supervisorActor: ActorRef = system.actorOf(
    BackoffSupervisor.props(Backoff.onFailure(
      ActorSupervisor.serverProps,
      "supervisor",
      3.seconds,
      3.minutes,
      0.2)), "backoff")

//  system.scheduler.scheduleOnce(20.seconds) {
//    system.actorOf(ActorSupervisor.serverProps, "supervisor")
//  }

  Http().bindAndHandle(fileManagement(), "localhost", 8888)
  //  system.registerOnTermination(closeCaches())
}











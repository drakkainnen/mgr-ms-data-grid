package com.mgr

import java.time.LocalDate

import akka.http.scaladsl.model.{ContentTypes, StatusCodes}
import akka.http.scaladsl.server.{Directives, MalformedRequestContentRejection}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.mgr.grid.Entities.FileInfo
import com.mgr.routes.Protocols
import org.scalatest.{FlatSpec, Matchers}
import spray.json.DeserializationException

/**
  * Created by BBUDNIK on 27.05.2017.
  */
class ProtocolsTest extends FlatSpec with Matchers with ScalatestRouteTest {

  var recivedData: FileInfo = _

  class MockJsonService extends Directives with Protocols {
    val route = {
      post {
        pathEndOrSingleSlash {
          entity(as[FileInfo]) { data =>
            recivedData = data
            complete(StatusCodes.OK)
          }
        }
      } ~ get {
        pathEndOrSingleSlash {
          complete(FileInfo(1, 1, "a", 1, LocalDate.parse("2017-05-27")))
          //          complete(StatusCodes.OK)
        }
      }
    }
  }

  val mock = new MockJsonService()

  it should "return correct data" in {
    Get("/") ~> mock.route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[String] shouldEqual """{"numberOfChunks":1,"fileName":"a","id":1,"creationDate":"2017-05-27","userId":1}"""
    }
  }

  it should "unmarshall FileInfo object" in {
    val postData =
      """{
        |"id":              1,
        |"userId":          1,
        |"fileName":        "a",
        |"numberOfChunks":  1,
        |"creationDate":    "2017-05-27"
        |}""".stripMargin

    Post("/").withEntity(ContentTypes.`application/json`, postData) ~> mock.route ~> check {
      status shouldBe StatusCodes.OK
      recivedData shouldEqual FileInfo(1, 1, "a", 1, LocalDate.parse("2017-05-27"))
    }
  }

  it should "complain for missing field" in {
    val postData =
      """{
        |"id":              1,
        |"userId":          1,
        |"fileName":        "a",
        |"numberOfChunks":  1
        |}""".stripMargin

    Post("/").withEntity(ContentTypes.`application/json`, postData) ~> mock.route ~> check {
      rejection shouldBe a[MalformedRequestContentRejection]
      rejection match {
        case MalformedRequestContentRejection(_, cause) =>
          cause shouldBe a[DeserializationException]
      }
    }
  }

}

package com.mgr

import java.time.LocalDate

import akka.actor.{Actor, ActorRef, Props, Status}
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.util.Timeout
import com.mgr.actors.FileInfoRepositoryHandlerComponent.{GetAll, GetFile}
import com.mgr.exceptions.FileExceptionHandler.FileInfoNotFound
import com.mgr.grid.Entities.FileInfo
import com.mgr.routes.FileManagementRoutes
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.duration._

/**
  * Created by BBUDNIK on 27.05.2017.
  */
class RoutesTest extends FlatSpec with Matchers with ScalatestRouteTest {

  var fileInfoId: Long = 0
  var fileName: String = ""

  private val mockActor = system.actorOf(Props(new Actor {
    override def receive: Receive = {
      case GetAll(userId) =>
        if (userId == 1) {
          sender() ! Seq(FileInfo(1, userId, "a", 1, LocalDate.parse("2017-05-27")))
        }
        sender() ! Status.Failure(FileInfoNotFound())
      case GetFile(userId, Some(name), None) =>
        if (userId == 1) {
          sender() ! FileInfo(1, userId, name, 1, LocalDate.parse("2017-05-27"))
          fileName = name
        }
        sender() ! Status.Failure(FileInfoNotFound())
      case GetFile(userId, None, Some(id)) =>
        if (userId == 1) {
          sender() ! FileInfo(1, userId, "someName", id, LocalDate.parse("2017-05-27"))
          fileInfoId = id
        }
        sender() ! Status.Failure(FileInfoNotFound())
      case o: Any =>
        sender() ! o
    }
  }))

  class MockService extends Directives with FileManagementRoutes {
    override implicit val supervisorActor: ActorRef = mockActor
    override implicit val timeout: Timeout = Timeout.apply(1.second)
  }

  val mock = new MockService()

  override def beforeAll(): Unit = {
    super.beforeAll()
    fileInfoId = 0
    fileName = ""
  }

  it should "call compress request" in {
    Get("/api/user/1/file-info/1/compress") ~> mock.fileManagement() ~> check {
      status shouldEqual StatusCodes.OK
    }
  }

  it should "call decompress request" in {
    Get("/api/user/1/file-info/1/decompress") ~> mock.fileManagement() ~> check {
      status shouldEqual StatusCodes.OK
    }
  }

  it should "call delete request" in {
    Delete("/api/user/1/file-info/1") ~> mock.fileManagement() ~> check {
      status shouldEqual StatusCodes.OK
    }
  }

  it should "call get all request" in {
    Get("/api/user/1/file-info") ~> mock.fileManagement() ~>
      check {
        status shouldEqual StatusCodes.OK
      }
  }

  it should "call get find request" in {
    Get("/api/user/1/file-info/10") ~> mock.fileManagement() ~> check {
      status shouldEqual StatusCodes.OK
      fileInfoId shouldEqual 10
    }
  }

  it should "call get by name request" in {
    Get("/api/user/1/file-info/name.txt") ~> mock.fileManagement() ~> check {
      status shouldEqual StatusCodes.OK
      fileName shouldEqual "name.txt"
    }
  }

  it should "return 404 when file with given name is not found" in {
    Get("/api/user/2/file-info/name.txt") ~> mock.fileManagement() ~> check {
      status shouldEqual StatusCodes.NotFound
      responseAs[String] shouldEqual "File was not found"
    }
  }
}


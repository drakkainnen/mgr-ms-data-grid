addSbtPlugin("io.spray" % "sbt-revolver" % "0.8.0")

// different packaging example
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.2.0-M9")

// fat jar
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.4")

//required by Kamon
//addSbtPlugin("com.typesafe.sbt" % "sbt-aspectj" % "0.10.0")


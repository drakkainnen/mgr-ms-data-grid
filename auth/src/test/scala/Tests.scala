import akka.actor.{Actor, ActorRef}
import akka.http.scaladsl.model.HttpHeader
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.headers.{BasicHttpCredentials, Cookie, HttpCookie}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.softwaremill.session.{InMemoryRefreshTokenStorage, SessionConfig, SessionManager}
import org.scalatest.{FlatSpec, Matchers, path}
import akka.http.scaladsl.model.headers.`Set-Cookie`
import akka.http.scaladsl.server.{AuthorizationFailedRejection, ExceptionHandler}
import akka.testkit.TestActorRef
import com.mgr.routes.AuthRoutes

import scala.concurrent.ExecutionContext

//class AuthorizationTest extends FlatSpec with Matchers with ScalatestRouteTest with AuthRoutes {
//
//  val conf = SessionConfig.default("12345678912345678912345678912345678912345678912345678912345678912345678912345678912345678900000000001234567890");
//
//  implicit val refreshTokenStorage = new InMemoryRefreshTokenStorage[Long] {
//    def log(msg: String) = println(msg)
//  }
//  override implicit val sessionManager: SessionManager[Long] = new SessionManager[Long](conf)
//  override implicit val userManagerActor: ActorRef = _
////  override implicit val exceptionHandler: ExceptionHandler = ExceptionHandler {
////    case d:Throwable =>
////      complete("")
////  }
////  override implicit val userDataManagement: ActorRef = null
//
//
//  it should "set session" in {
//    val credentials = BasicHttpCredentials("b", "b")
//    Get("/api/login") ~> addCredentials(credentials) ~> authRoutes ~> check {
//      status shouldBe OK
//      responseAs[String] shouldEqual "12"
//
//      header[`Set-Cookie`].get.cookie.name shouldEqual "_sessiondata"
//      header[`Set-Cookie`].get.cookie.value should endWith("x12")
//    }
//  }
//
//  it should "return 403 when session not set" in {
//    Get("/api/logout") ~> authRoutes ~> check {
//      rejection shouldEqual AuthorizationFailedRejection
//    }
//  }
//
//  it should "invalidate session" in {
//    val credentials = BasicHttpCredentials("b", "b")
//    Get("/api/login") ~> addCredentials(credentials) ~> authRoutes ~> check {
//      val cookie = header[`Set-Cookie`].get.cookie
//
//      Get("/api/logout") ~> addHeader(Cookie(cookie.name, cookie.value)) ~> authRoutes ~> check {
//        responseAs[String] shouldEqual "logged off"
//        header[`Set-Cookie`].get.cookie.value shouldEqual "deleted"
//      }
//    }
//  }
//
//  it should "be forbidden" in {
//    Get("/api/require") ~> authRoutes ~> check {
//      rejection shouldEqual AuthorizationFailedRejection
//    }
//  }
//}

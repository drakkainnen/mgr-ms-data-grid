package com.mgr.grid.client

import com.hazelcast.Scala.client._
import com.mgr.grid.HzComponent

/**
  * Created by BBUDNIK on 29.05.2017.
  */
trait HzClientComponent extends HzComponent {
  this: HzClientConfig =>

  val hzInstance = config.newClient()
}

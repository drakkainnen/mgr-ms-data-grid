package com.mgr.grid.client

import com.hazelcast.client.config.ClientConfig

trait HzClientConfig {
  val config:ClientConfig
}

trait DefaultHzClientConfig extends HzClientConfig {
  override val config = new ClientConfig
}


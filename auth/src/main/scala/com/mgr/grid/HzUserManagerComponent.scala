package com.mgr.grid

import akka.Done
import com.hazelcast.core.IMap
import com.hazelcast.util.MD5Util
import com.mgr.actors.UserManagerComponent
import com.mgr.routes.UserData

/**
  * Created by BBUDNIK on 29.05.2017.
  */
trait HzUserManagerComponent extends UserManagerComponent {
  this: HzComponent =>

  override val userManager = new HzUserManager

  class HzUserManager extends UserManager {

    import HzUserManagerComponent._
    import akka.http.scaladsl.server.directives.Credentials
    import com.hazelcast.core.{IMap, IdGenerator}
    import com.hazelcast.query.Predicates
    import com.hazelcast.util.MD5Util
    import com.mgr.exceptions.UserExceptionHandler.{PasswordDoesNotMatchException, UserAlreadyChangedException, UserExistsException, UserNotFoundException}

    import scala.collection.JavaConverters._

    val userCache: IMap[Long, UserData] = hzInstance.getMap(usercache)
    val idGenerator: IdGenerator = hzInstance.getIdGenerator(usergenerator)

    override def initialize: Unit = {
      val data = UserData("admin", "admin", "admin", MD5Util.toMD5String("admin"))
      userCache.put(1L, data)
    }

    override def changePassword(id: Long, oldPassword: String, newPassword: String): Any = {
      val userData = userCache.get(id)
      if (userData.password equals oldPassword) {
        val replace = userCache.replace(id, userData, userData.copy(password = newPassword))
        if (replace) {
          Done
        } else {
          UserAlreadyChangedException()
        }
      } else {
        PasswordDoesNotMatchException()
      }
    }

    override def createUser(userData: UserData): Any = {
      val predicate = Predicates.equal("username", userData.username)
      val values = userCache.values(predicate).asScala

      if (values.isEmpty) {
        save(userData.copy(password = MD5Util.toMD5String(userData.password)))
      } else {
        UserExistsException()
      }
    }

    private def save(userData: UserData): Any = {
      val userCreated = Option(userCache.putIfAbsent(idGenerator.newId(), userData))
      userCreated match {
        case Some(_) =>
          UserExistsException()
        case None =>
          Done
      }
    }

    override def deleteUser(id: Long): Any = {
      val userData = Option(userCache.remove(id))
      userData match {
        case Some(_) =>
          Done
        case None =>
          UserNotFoundException()
      }
    }

    override def getUserData(id: Long): Any = {
      val userData = Option(userCache.get(id))
      userData match {
        case Some(data) =>
          data.copy(password = "")
        case None =>
          UserNotFoundException()
      }
    }

    override def checkCredentials(credentials: Credentials.Provided): Option[Long] = {
      val identifier = credentials.identifier
      val predicate = Predicates.equal("username", identifier)

      val results = userCache.entrySet(predicate).asScala
      results.headOption
        .filter(u => credentials.verify(u.getValue.password, MD5Util.toMD5String))
        .map(_.getKey())
    }

    initialize
  }

}

object HzUserManagerComponent {
  val usercache = "userCache"
  val usergenerator = "userGenerator"
}
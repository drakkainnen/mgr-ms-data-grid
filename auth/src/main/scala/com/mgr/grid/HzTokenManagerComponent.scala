package com.mgr.grid

import com.hazelcast.core.IMap
import com.mgr.actors.TokenManagerComponent
import com.mgr.grid.Entities.UserSession

trait HzTokenManagerComponent[T] extends TokenManagerComponent[T] {
  this: HzComponent =>
  override val tokenManager = new HzTokenManager[T]

  class HzTokenManager[T] extends TokenManager[T] {
    val store: IMap[String, UserSession[T]] = hzInstance.getMap[String, UserSession[T]](HzTokenManagerComponent.sessioncache)

    def removeToken(selector: String) = {
      store.remove(selector)
    }

    def storeToken(selector: String, session: UserSession[T]) = {
      store.put(selector, session)
    }

    def findToken(selector: String) = {
      Some(store.get(selector))
    }
  }

}

object HzTokenManagerComponent {
  val sessioncache = "sessionCache"
}

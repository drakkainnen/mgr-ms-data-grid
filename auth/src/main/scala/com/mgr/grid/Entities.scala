package com.mgr.grid

object Entities {

  case class UserSession[T](session: T, tokenHash: String, expires: Long)

}

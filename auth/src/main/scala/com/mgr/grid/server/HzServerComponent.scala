package com.mgr.grid.server

import com.hazelcast.Scala._
import com.mgr.grid.HzComponent

/**
  * Created by BBUDNIK on 29.05.2017.
  */
trait HzServerComponent extends HzComponent {
  this: HzServerConfig =>


  val hzInstance = hzConfig.newInstance()
}

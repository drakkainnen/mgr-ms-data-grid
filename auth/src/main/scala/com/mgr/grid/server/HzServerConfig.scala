package com.mgr.grid.server

import com.hazelcast.config.Config

trait HzServerConfig {
  val hzConfig: Config
}

trait DefaultHzServerConfig extends HzServerConfig {
  override val hzConfig = new Config
}

trait CustomHzServerConfig extends HzServerConfig {

  import com.hazelcast.config.{Config, MapConfig, MapIndexConfig}

  private val userCacheConfig = new MapConfig()
    .setName("userCache")
    .addMapIndexConfig(new MapIndexConfig("username", true))
  println("setting cache config")

  override val hzConfig = new Config().addMapConfig(userCacheConfig)
}


package com.mgr.routes

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

case class UserData(username: String, firstName: String, lastName: String, password: String = null) {
  require(username.nonEmpty, "Username must be set")
  require(username.length > 4, "Username must contain at least 5 characters")
  require(firstName.nonEmpty, "First name must be set")
  require(lastName.nonEmpty, "Last name must be set")
}

trait Protocols extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val userDataFormat: RootJsonFormat[UserData] = jsonFormat4(UserData)
}

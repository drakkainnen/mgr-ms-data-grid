package com.mgr.routes

import akka.Done
import akka.actor.ActorSelection
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.directives.Credentials
import akka.http.scaladsl.server.{ExceptionHandler, Route}
import akka.pattern.ask
import akka.util.Timeout
import com.mgr.actors.UserManagerHandlerComponent.{CreateUser, DeleteUser, GetUserData}
import com.softwaremill.session.SessionDirectives.{invalidateSession, requiredSession, setSession}
import com.softwaremill.session.SessionManager
import com.softwaremill.session.SessionOptions.{oneOff, usingCookies}

import scala.concurrent.Future
import scala.concurrent.duration._

trait AuthRoutes extends Protocols {
  implicit val sessionManager: SessionManager[Long]
  implicit val userManager: ActorSelection

  def authRoutes(exceptionHandler: ExceptionHandler, timeoutDuration: FiniteDuration): Route = pathPrefix("api") {
    //istotnie wplywa na przetwarzanie zadan
    implicit val timeout: Timeout = Timeout(timeoutDuration)
    handleExceptions(exceptionHandler) {
      path("login") {
        authenticateBasicAsync(realm = "securedsection", authernticator) {
          userData =>
            setSession[Long](oneOff, usingCookies, userData) {
              complete(userData.toString)
            }
        }
      } ~ path("logout") {
        get {
          requiredSession[Long](oneOff, usingCookies) { _ =>
            invalidateSession(oneOff, usingCookies) {
              complete(s"logged off")
            }
          }
        }
      } ~ path("user") {
        get {
          requiredSession[Long](oneOff, usingCookies) { userId =>
            val future = userManager ? GetUserData(userId)
            onSuccess(future) {
              case e: Throwable =>
                throw e
              case userData: UserData =>
                complete(userData)
            }
          }
        }
      } ~ path("user") {
        post {
          entity(as[UserData]) { userData =>
            onSuccess(userManager ? CreateUser(userData)) {
              case e: Throwable =>
                throw e
              case Done =>
                complete(StatusCodes.OK)
            }
          }
        }
      } ~ path("user") {
        delete {
          requiredSession[Long](oneOff, usingCookies) { userId =>
            onSuccess(userManager ? DeleteUser(userId)) {
              case e: Throwable =>
                throw e
              case Done =>
                invalidateSession(oneOff, usingCookies) {
                  complete(StatusCodes.OK)
                }
            }
          }
        }
      } ~ path("remote") {
        complete("122")
      }
    }
  }

  def authernticator(credentials: Credentials): Future[Option[Long]] = {
    implicit val timeout = Timeout(200.seconds)
    credentials match {
      case c@Credentials.Provided(_) =>
        userManager.ask(c).mapTo[Option[Long]]
      case _ =>
        Future.successful(None)
    }
  }
}

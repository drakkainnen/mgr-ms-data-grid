package com.mgr.actors

import akka.actor.{Actor, Props}
import akka.routing.FromConfig
import com.mgr.db.DbUserManagerComponent
import com.typesafe.config.Config
import slick.jdbc.H2Profile.api._

import scala.concurrent.ExecutionContextExecutor

object DbSupervisor {
  def props(config: Config): Props = Props(new DbSupervisor)
}

class DbSupervisor extends Actor {
  val h2db = Database.forConfig("h2db", context.system.settings.config)

  private val component = new UserManagerHandlerComponent
    with DbUserManagerComponent {
    override implicit val db = h2db
    override implicit val ex: ExecutionContextExecutor = context.dispatcher
  }

  component.userManager.initialize
  private val userManagerActor = context.actorOf(FromConfig.props(component.asyncUserManagerHandlerProps), "userManagerActor")

  override def receive: Receive = {
    case d =>
      userManagerActor forward d
  }
}

package com.mgr.actors

import akka.Done
import com.mgr.routes.UserData

import scala.concurrent.Future

trait UserManagerComponent {

  import akka.http.scaladsl.server.directives.Credentials

  val userManager: UserManager

  trait UserManager {
    def initialize: Unit

    def changePassword(id: Long, oldPassword: String, newPassword: String): Any

    def createUser(userData: UserData): Any

    def deleteUser(id: Long): Any

    def getUserData(id: Long): Any

    def checkCredentials(credentials: Credentials.Provided): Any
  }

}

object UserManagerHandlerComponent {

  case class CreateUser(userData: UserData)

  case class DeleteUser(id: Long)

  case class ChangePassword(id: Long, oldPassword: String, newPassword: String)

  case class GetUserData(id: Long)

}

trait UserManagerHandlerComponent {
  this: UserManagerComponent =>

  import akka.actor.{Actor, ActorLogging, Props}
  import akka.http.scaladsl.server.directives.Credentials

  def userManagerHandlerProps: Props = Props(new UserManagerActor())
  def asyncUserManagerHandlerProps: Props = Props(new AsyncUserManagerActor())

  class UserManagerActor extends Actor with ActorLogging {

    import UserManagerHandlerComponent._

    log.info(s"New actor created: ${self.path.toString}")

    override def receive: Receive = {
      case CreateUser(userData) =>
        sender() ! userManager.createUser(userData)
      case DeleteUser(id) =>
        sender() ! userManager.deleteUser(id)
      case ChangePassword(id, oldPassword, newPassword) =>
        sender() ! userManager.changePassword(id, oldPassword, newPassword)
      case GetUserData(id) =>
        sender() ! userManager.getUserData(id)
      case c@Credentials.Provided(_) =>
        sender() ! userManager.checkCredentials(c)
      case _ =>
    }
  }

  class AsyncUserManagerActor extends Actor with ActorLogging {

    import akka.pattern.pipe
    import UserManagerHandlerComponent._

    implicit val ex = context.dispatcher

    log.info(s"New async actor created: ${self.path.toString}")

    override def receive: Receive = {
      case CreateUser(userData) =>
        sender() ! userManager.createUser(userData)
      case DeleteUser(id) =>
        userManager.deleteUser(id).asInstanceOf[Future[Any]] pipeTo sender()
      case ChangePassword(id, oldPassword, newPassword) =>
        userManager.changePassword(id, oldPassword, newPassword).asInstanceOf[Future[Any]] pipeTo sender()
      case GetUserData(id) =>
        userManager.getUserData(id).asInstanceOf[Future[Any]] pipeTo sender()
      case c@Credentials.Provided(_) =>
        userManager.checkCredentials(c).asInstanceOf[Future[Any]] pipeTo sender()
      case _ =>
    }
  }

}






package com.mgr.actors

import akka.actor.{Actor, ActorLogging, Props}
import com.mgr.grid.Entities.UserSession

trait TokenManagerComponent[T] {
  val tokenManager: TokenManager[T]

  trait TokenManager[T] {
    def removeToken(selector: String)

    def storeToken(selector: String, session: UserSession[T])

    def findToken(selector: String)
  }

}

object TokenManagerHandlerComponent {

  case class FindSession(selector: String)

  case class StoreSession[T](selector: String, session: UserSession[T])

  case class RemoveSession(selector: String)

}

trait TokenManagerHandlerComponent[T] {
  this: TokenManagerComponent[T] =>

  def props(): Props = Props(new TokenManagerHandler)

  class TokenManagerHandler extends Actor with ActorLogging {

    import com.mgr.actors.TokenManagerHandlerComponent.{FindSession, RemoveSession, StoreSession}

    log.debug(context.dispatcher.toString)

    override def receive: Receive = {
      case FindSession(selector) =>
        tokenManager.findToken(selector)
      case c:StoreSession[T] =>
        tokenManager.storeToken(c.selector, c.session)
      case RemoveSession(selector) =>
        tokenManager.removeToken(selector)
    }
  }

}





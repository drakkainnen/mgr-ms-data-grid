package com.mgr.actors

import akka.actor.SupervisorStrategy._
import akka.actor.{Actor, ActorLogging, AllForOneStrategy, Props}
import akka.routing.FromConfig
import com.mgr.grid.client.{DefaultHzClientConfig, HzClientComponent}
import com.mgr.grid.server.{CustomHzServerConfig, HzServerComponent}
import com.mgr.grid.{HzComponent, HzTokenManagerComponent, HzUserManagerComponent}

import scala.concurrent.duration._

object StorageType extends Enumeration {
  type StorageType = Value
  val hzClient, hzServer, dbClient = Value
}

object HazelcastSupervisor {

  def props(client: Boolean) = if (client) clientProps() else serverProps()

  def serverProps(): Props = Props(new HazelcastSupervisor[Long]
    with CustomHzServerConfig with HzServerComponent
    with HzTokenManagerComponent[Long] with TokenManagerHandlerComponent[Long]
    with HzUserManagerComponent with UserManagerHandlerComponent)

  def clientProps(): Props = Props(new HazelcastSupervisor[Long]
    with DefaultHzClientConfig with HzClientComponent
    with HzTokenManagerComponent[Long] with TokenManagerHandlerComponent[Long]
    with HzUserManagerComponent with UserManagerHandlerComponent)
}

class HazelcastSupervisor[T] extends Actor with ActorLogging {
  this: HzComponent
    with TokenManagerComponent[T] with TokenManagerHandlerComponent[T]
    with UserManagerComponent with UserManagerHandlerComponent =>

  override val supervisorStrategy = AllForOneStrategy(maxNrOfRetries = 10,
    withinTimeRange = 1.minute,
    loggingEnabled = true) {
    case _: IllegalStateException => Resume
    case _: NullPointerException => Resume
    case _: Exception => Escalate
  }

  private val userManagerActor = context.actorOf(FromConfig.props(userManagerHandlerProps), "userManagerActor")

  override def receive: Receive = {
    case d =>
      userManagerActor forward d
  }
}

package com.mgr.db

import slick.jdbc.H2Profile.api._

class Users(tag: Tag) extends Table[(Int, String, String, String, String)](tag, "USERS") {
  def id = column[Int]("ID", O.PrimaryKey, O.AutoInc) // This is the primary key column
  def username = column[String]("USERNAME")

  def firstName = column[String]("FIRST_NAME")

  def lastName = column[String]("LAST_NAME")

  def password = column[String]("PASSWORD")

  // Every table needs a * projection with the same type as the table's type parameter
  def * = (id, username, firstName, lastName, password)
  def idx = index("username_idx", username, true)
}

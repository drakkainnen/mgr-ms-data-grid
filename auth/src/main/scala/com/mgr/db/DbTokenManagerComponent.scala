package com.mgr.db

import com.mgr.actors.TokenManagerComponent
import com.mgr.grid.Entities

/**
  * Created by BBUDNIK on 26.06.2017.
  */
trait DbTokenManagerComponent[T] extends TokenManagerComponent[T] {

  override val tokenManager = new DbTokenManager[T]

  class DbTokenManager[T] extends TokenManager[T] {
    override def removeToken(selector: String): Unit = ???

    override def storeToken(selector: String, session: Entities.UserSession[T]): Unit = ???

    override def findToken(selector: String): Unit = ???
  }
}

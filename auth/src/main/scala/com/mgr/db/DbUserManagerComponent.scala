package com.mgr.db

import akka.Done
import akka.http.scaladsl.server.directives.Credentials
import com.hazelcast.util.MD5Util
import com.mgr.actors.UserManagerComponent
import com.mgr.exceptions.UserExceptionHandler.UserExistsException
import com.mgr.routes.UserData
import slick.jdbc.H2Profile
import slick.jdbc.H2Profile.api._
import slick.jdbc.meta.MTable
import slick.lifted.TableQuery

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContextExecutor, Future}

trait DbUserManagerComponent extends UserManagerComponent {
  implicit val db: H2Profile.backend.Database
  override val userManager = new DbUserManager

  implicit val ex: ExecutionContextExecutor

  class DbUserManager extends UserManager {
    val users: TableQuery[Users] = TableQuery[Users]

    override def initialize: Unit = {

      val eventualInt = db.run(MTable.getTables("USERS").map(m => m.isEmpty)) flatMap {
        case false =>
          Future.successful()
        case true =>
          db.run(users.schema.create)
      } flatMap { u =>
        val existing = users.filter(c => c.username === "admin").map(_.id).result.headOption
        db.run(existing) flatMap {
          case None =>
            db.run(users += (1, "admin", "admin", "admin", MD5Util.toMD5String("admin")))
          case Some(id) =>
            Future.successful(0)
        }
      }
      Await.result(eventualInt, Duration.Inf)
    }

    override def changePassword(id: Long, oldPassword: String, newPassword: String): Future[Any] = ???

    override def createUser(userData: UserData): Any = {
      val entriesWithGivenName = users.filter(u => u.username === userData.username).length.result
      val insertq = users += (1, userData.username, userData.firstName, userData.lastName, MD5Util.toMD5String(userData.password))

      val eventualObject = db.run(entriesWithGivenName) flatMap { l =>
        if (l == 0) {
          db.run(insertq)
        }
        else {
          Future.successful(0)
        }
      }
      val result = Await.result(eventualObject, Duration.Inf)
      if(result == 0) UserExistsException() else Done
    }

    override def deleteUser(id: Long): Future[Any] = ???

    override def getUserData(id: Long): Future[Any] = ???

    override def checkCredentials(credentials: Credentials.Provided): Future[Option[Long]] = ???
  }

}


package com.mgr.exceptions

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives.complete
import akka.http.scaladsl.server.ExceptionHandler

/**
  * Created by BBUDNIK on 02.05.2017.
  */
object UserExceptionHandler {

  case class UserExistsException() extends RuntimeException("User already exists")

  case class UserAlreadyChangedException() extends RuntimeException("User already changed")

  case class PasswordDoesNotMatchException() extends RuntimeException("Password does not match")

  case class UserNotFoundException() extends RuntimeException("User not found")

  def apply(): ExceptionHandler = ExceptionHandler {
    case e: UserExistsException =>
      complete(StatusCodes.BadRequest, e.getMessage)
    case e: UserAlreadyChangedException =>
      complete(StatusCodes.InternalServerError, e.getMessage)
    case e: PasswordDoesNotMatchException =>
      complete(StatusCodes.BadRequest, e.getMessage)
    case e: UserNotFoundException =>
      complete(StatusCodes.NotFound, e.getMessage)
  }
}

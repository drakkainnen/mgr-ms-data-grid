package com.mgr

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSelection, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.pattern.{Backoff, BackoffSupervisor}
import akka.stream.ActorMaterializer
import com.mgr.actors.{DbSupervisor, HazelcastSupervisor, StorageType}
import com.mgr.exceptions.UserExceptionHandler
import com.mgr.routes.AuthRoutes
import com.mgr.util.ActorRefreshTokenStorage
import com.softwaremill.session._
import com.typesafe.config.ConfigFactory

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._

object AuthMicroservice
  extends App
    with AuthRoutes {

  private val defaultConfig = ConfigFactory.load()
  println(defaultConfig.getString("akka.deploy").toUpperCase)

  implicit val system: ActorSystem = ActorSystem("auth-system", defaultConfig)
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  val supervisorProps = StorageType.withName(defaultConfig.getString("storage-type")) match {
    case StorageType.hzClient =>
      HazelcastSupervisor.props(true)
    case StorageType.hzServer =>
      HazelcastSupervisor.props(false)
    case StorageType.dbClient =>
      DbSupervisor.props(defaultConfig)
  }

  implicit val hzSupervisor: ActorRef = system.actorOf(
    BackoffSupervisor.props(
      Backoff.onStop(
        supervisorProps,
        "hzSupervisor",
        3.seconds,
        3.minutes,
        0.2
      )
    ), "backoff")


  implicit val userManager: ActorSelection = system.actorSelection("/user/backoff/hzSupervisor/userManagerActor")

  private val sessionConfig = SessionConfig.default("12345678912345678912345678912345678912345678912345678912345678912345678912345678912345678900000000001234567890")
  implicit val sessionManager = new SessionManager[Long](sessionConfig)
  implicit val refreshTokenStorage = new ActorRefreshTokenStorage[Long](hzSupervisor)

  Http().bindAndHandle(authRoutes(
    UserExceptionHandler(),
    FiniteDuration(defaultConfig.getLong("query-timeout"), "millis")),
    defaultConfig.getString("http.host"),
    defaultConfig.getInt("http.port"))
}











enablePlugins(JavaAppPackaging)

name := "auth"
version := "1.0"
scalaVersion := "2.11.8"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

resolvers ++= Seq(Resolver.jcenterRepo, Resolver.bintrayRepo("clockfly", "maven"))

mainClass in Compile := Some("com.mgr.AuthMicroservice")

libraryDependencies ++= {
  val akkaV = "2.5.2"
  val akkaHttpV = "10.0.6"
  val scalaTestV = "3.0.1"
  val slickVersion = "3.2.0"
  val h2Version = "1.4.195"

  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaV,
    "com.typesafe.akka" %% "akka-stream" % akkaV,
    "com.typesafe.akka" %% "akka-remote" % akkaV,

    "com.typesafe.akka" %% "akka-http" % akkaHttpV,
    "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpV,

    "com.hazelcast" % "hazelcast" % "3.8.1",
    "com.hazelcast" % "hazelcast-client" % "3.8.1",
    "com.hazelcast" %% "hazelcast-scala" % "3.7.2",

    "com.softwaremill.akka-http-session" %% "core" % "0.4.0",

    "com.typesafe.slick" %% "slick" % slickVersion,
    "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
    "com.h2database" % "h2" % h2Version,

    "org.scalatest" %% "scalatest" % scalaTestV % "test",
    "com.typesafe.akka" %% "akka-testkit" % akkaV % "test",
    "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpV % "test"
  )
}
Revolver.settings




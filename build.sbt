lazy val root = project.in( file(".") )
  .aggregate(fileGateway, auth, webGateway, fileManagement)
  .settings(
    aggregate in update := false
  )

lazy val fileGateway = project.in( file("file-gateway"))

lazy val auth = project.in( file("auth"))

lazy val webGateway = project.in( file("web-gateway"))

lazy val fileManagement = project.in( file("file-management"))
